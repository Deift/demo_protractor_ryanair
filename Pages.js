var Pages = function () {

    var HomePage = new require("./pages/HomePage.js");
    var SelectFlightPage = new require("./pages/SelectFlightPage.js");
    var ExtrasPage = new require("./pages/ExtrasPage.js");

    this.homePage = new HomePage();
    this.selectFlightPage = new SelectFlightPage();
    this.extrasPage = new ExtrasPage();

};

module.exports = Pages;