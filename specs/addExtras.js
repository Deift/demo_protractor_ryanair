describe('Protractor Demo', function () {
    beforeEach(function () {
        browser.get('https://www.ryanair.com/gb/en/');
    });

    it('I can add bags', function () {
        actions.homeActions.searchFlight('DUB', 'STN',3,0,0,0);
        actions.selectFlightActions.selectFlight();
        actions.extrasActions.addBags();
    });

    it('I can add seats', function () {
        actions.homeActions.searchFlight('DUB', 'STN',3,0,0,0);
        actions.selectFlightActions.selectFlight();
        actions.extrasActions.addSeats();
    });

});
