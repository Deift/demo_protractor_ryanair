var ExtrasActions = function () {

    var extrasPage = pages.extrasPage;

    this.addBags = function(){
        browser.wait(EC.presenceOf(extrasPage.btnAddBags()),3000);
        extrasPage.btnAddBags().click();
        extrasPage.listBtnPlusBags().get(0).click();
        extrasPage.btnConfirmBags().click();
        expect(extrasPage.textBagsAdded().isPresent()).toBe(true);
    };

    this.addSeats = function(){
        extrasPage.btnAddSeats().click();
        browser.wait(EC.presenceOf(extrasPage.listBtnSeat().get(0)),3000)
        extrasPage.listBtnSeat().get(0).click();
        extrasPage.btnNextSeats().click();
        browser.wait(EC.presenceOf(extrasPage.btnSameSeatsReturn()),3000).then(function(){
            extrasPage.btnSameSeatsReturn().click();
        },function(){
            extrasPage.listBtnSeat().get(0).click();
            extrasPage.btnNextSeats().click();
        });
        extrasPage.btnConfirmSeats().click();
        expect(extrasPage.textSeatsAdded().isPresent()).toBe(true);
    };

};

module.exports = ExtrasActions;