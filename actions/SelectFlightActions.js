var SelectFlightActions = function () {

    this.selectFlight = function () {
        browser.wait(EC.presenceOf(pages.selectFlightPage.fareOut()),5000);
        pages.selectFlightPage.fareOut().click();
        pages.selectFlightPage.fareIn().click();
        pages.selectFlightPage.btnContinue().click();
    };

};

module.exports = SelectFlightActions;