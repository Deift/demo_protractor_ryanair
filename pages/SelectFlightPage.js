var SelectFlightPage = function () {

    this.fareOut = function () {
        return element.all(by.css('#outbound .regular')).get(0);
    };

    this.fareIn = function () {
        return element.all(by.css('#inbound .regular')).get(0);
    };

    this.btnContinue = function () {
        return element(by.css('[translate="trips.summary.buttons.btn_continue"]'));
    };

};

module.exports = SelectFlightPage;