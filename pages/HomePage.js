var HomePage = function () {

    this.AirportOut = function () {
        return element.all(by.css('input[ng-model="$parent.value"]')).get(0);
    };

    this.AirportIn = function () {
        return element.all(by.css('input[ng-model="$parent.value"]')).get(1);
    };

    this.calendarOut = function () {
        return element(by.css('.col-cal-from'));
    };

    this.dateOut = function (date) {
        return element(by.css('.start-date [data-id="' + date + '"]'));
    };

    this.calendarIn = function () {
        return element(by.css('.col-cal-to'));
    };

    this.dateIn = function (date) {
        return element(by.css('.end-date [data-id="' + date + '"]'));
    };

    this.passegersDropdown = function () {
        return element(by.css('.col-passengers'));
    };

    this.passegersAdults = function () {
        return element.all(by.css('[ng-click="$ctrl.increment()"]')).get(0);
    };

    this.passegersTeens = function () {
        return element.all(by.css('[ng-click="$ctrl.increment()"]')).get(1);
    };

    this.passegersChildren = function () {
        return element.all(by.css('[ng-click="$ctrl.increment()"]')).get(2);
    };

    this.passegersInfants = function () {
        return element.all(by.css('[ng-click="$ctrl.increment()"]')).get(3);
    };

    this.btnLetsGo = function () {
        return element(by.css('[translate="common.buttons.lets_go"]'));
    };

};

module.exports = HomePage;
