var ExtrasPage = function () {

    this.btnAddBags = function () {
        return element(by.css('[translate="trips.extras.leaderboard.quick-card.BAGS.button.select"]'));
    };

    this.listBtnPlusBags = function () {
        return element.all(by.css('[ng-click="$ctrl.increment()"]'));
    };

    this.btnConfirmBags = function () {
        return element(by.css('[translate="trips.side.confirm"]:nth-child(1)'));
    };

    this.textBagsAdded = function () {
        return element(by.css('[translate="trips.extras.leaderboard.quick-card.BAGS.added"]'));
    };

    //seats

    this.btnAddSeats = function () {
        return element(by.css('[translate="trips.extras.leaderboard.quick-card.SEATS.button.select"]'));
    };

    this.listBtnSeat = function () {
        return element.all(by.css('.seat-row-seat:not(.aisle):not(.reserved) .seat-click'));
    };

    this.btnNextSeats = function () {
        return element(by.css('[translate="trips.seats.next"]'));
    };

    this.btnSameSeatsReturn = function () {
        return element(by.css('[translate="trips.seats.same_seats_yes"]'));
    };

    this.btnConfirmSeats = function () {
        return element(by.css('[translate="trips.seats.confirm"]'));
    };

    this.btnRefusePB = function () {
        return element(by.css('[translate="trips.seats.pb.noaddpb"]:nth-child(2)'));
    };

    this.textSeatsAdded = function () {
        return element(by.css('[translate="trips.extras.leaderboard.quick-card.SEATS.added"]'));
    };


};

module.exports = ExtrasPage;