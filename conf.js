exports.config = {

    onPrepare: function(){
        var Pages = new require("./Pages.js");
        global.pages = new Pages();
        var Actions = new require("./Actions.js");
        global.actions = new Actions();
        global.moment = require('moment');
        global.EC = protractor.ExpectedConditions;
        browser.driver.manage().window().maximize();
    },

    // Capabilities to be passed to the webdriver instance.
    capabilities: {
        'browserName': 'chrome'
    },
    //seleniumAddress: 'http://localhost:4444/wd/hub',
    framework: 'jasmine',
    specs: ['./specs/addExtras.js'],
    jasmineNodeOpts : {
        showColors: true,
        defaultTimeoutInterval : 200000
    }
};